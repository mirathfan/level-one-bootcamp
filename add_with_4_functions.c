//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
float num(char* h)
{
    float n;
    printf("Enter the %s number =\n",h);
    scanf("%f",&n);
    return n;
}

float sum(float x,float y)
{
	float s;
	s=x+y;
	return s;
}
void output(float p,float q, float r)
{
	printf("The sum of the two numbers %f and %f is =%f\n",p,q,r);
}
int main()
{
	float a,b,c,d;
	a=num("first");
	b=num("second");
	c=sum(a,b);
	output(a,b,c);
	return 0;
}