//WAP to find the sum of two fractions.
#include <stdio.h>
struct fraction
{
    float n,d;
};
typedef struct fraction Fraction;
Fraction get()
{
    Fraction F;
    printf("Enter numerator ");
    scanf("%f",&F.n);
    printf("Enter denominator ");
    scanf("%f",&F.d);
    return F;
}
float compute(Fraction F1,Fraction F2)
{
    float s;
    s=(F1.n/F1.d)+(F2.n/F2.d);
    return s;
}
void output(Fraction F1, Fraction F2, float sum)
{
    printf("The sum of %f/%f and %f/%f is %f",F1.n,F1.d,F2.n,F2.d,sum);
}
int main(void)
{
    float sum;
    Fraction f1,f2;
    f1=get();
    f2=get();
    sum=compute(f1,f2);
    output(f1,f2,sum);
    return 0;
}