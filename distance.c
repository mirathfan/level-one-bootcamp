//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
float get(char a,int b)
{
    float v;
    printf("Enter the value of co-ordinate %c%d:\n",a,b);
    scanf("%f",&v);
    return v;
}
float compute(float l,float m,float n, float o)
{
    float d;
    d=sqrt(pow((n-l),2)+pow((o-m),2)) ;
    return d;
}
void output(float p,float q,float r,float s,float t)
{
    printf("The distance between the two points (%f,%f) and (%f,%f) is =%f\n",p,q,r,s,t);
}
int main()
{
    float x1,y1,x2,y2,c,e;
    x1=get('x',1);
    y1=get('y',1);
    x2=get('x',2);
    y2=get('y',2);
    c=compute(x1,y1,x2,y2);
    output(x1,y1,x2,y2,c);
    return 0;
}